package com.bieber.mycat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by bieber on 2015/9/20.
 */
public class ConcurrentSequence {

    public static void main(String[] args) throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        for(int i=0;i<100;i++){
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Connection connection = DriverManager.getConnection("jdbc:mysql://192.168.1.6:8066/TESTDB?user=test&password=test&useUnicode=true&characterEncoding=UTF8");
                        Statement statement = connection.createStatement();
                        statement.execute("insert into t_users (id,province_code) values(next value for MYCATSEQ_USERS,\"110000\");");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
        }
    }

}
