package com.bieber.dubbo.extension;

import com.alibaba.dubbo.common.extension.ExtensionLoader;

/**
 * Created by bieber on 2015/5/23.
 */
public class ExtensionTest {

    public static void main(String[] args){
       ExtensionLoader extensionLoader = ExtensionLoader.getExtensionLoader(MyFirstExtension.class);
        MyFirstExtension myFirstExtension = (MyFirstExtension) extensionLoader.getAdaptiveExtension();
        System.out.println(myFirstExtension.sayHello("bieber",ExtensionType.DEFAULT));
    }
}
