package com.bieber.dubbo.extension;

import com.alibaba.dubbo.common.extension.SPI;

/**
 * Created by bieber on 2015/5/23.
 */
@SPI("default")
public interface MyFirstExtension {

    public String sayHello(String name,ExtensionType type);

}
