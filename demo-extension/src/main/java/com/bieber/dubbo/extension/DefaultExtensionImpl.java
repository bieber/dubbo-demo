package com.bieber.dubbo.extension;

/**
 * Created by bieber on 2015/5/23.
 */
public class DefaultExtensionImpl implements MyFirstExtension {
    @Override
    public String sayHello(String name,ExtensionType type) {
        return "default say hello "+name;
    }
}
