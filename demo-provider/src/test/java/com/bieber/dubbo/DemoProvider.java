package com.bieber.dubbo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Created by bieber on 2015/4/30.
 */
public class DemoProvider {

    public static  void main(String[] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring/*.xml");
        System.out.println("Service started.....");
        System.in.read();
    }
}
