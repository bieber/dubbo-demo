package com.bieber.dubbo.service;

import com.bieber.dubbo.model.User;

/**
 * Created by bieber on 2015/4/30.
 */
public class MyFirstDubboServiceImpl implements MyFirstDubboService {
    @Override
    public byte[] sayHello(byte[] message) {
        return message;
    }
}
